<?php

namespace Drupal\drd\Plugin\Action;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\drd\ActionManagerInterface;
use Drupal\drd\SelectEntitiesInterface;
use Drupal\drd\HttpRequest;
use Drupal\drd\Logging;
use Drupal\drd\QueueManager;
use Drupal\drd\UpdateProcessor;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for DRD Action plugins.
 */
abstract class Base extends ActionBase implements BaseInterface, ConfigurableInterface, PluginFormInterface, ContainerFactoryPluginInterface {

  /**
   * Action arguments.
   *
   * @var array
   */
  protected $arguments = [];

  /**
   * Action output.
   *
   * @var string|string[]
   */
  protected $output;

  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\drd\SelectEntitiesInterface
   */
  protected $entities;

  /**
   * @var \Drupal\drd\HttpRequest
   */
  protected $httpRequest;

  /**
   * @var \Drupal\drd\Logging
   */
  protected $logging;

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\drd\ActionManagerInterface
   */
  protected $actionManager;

  /**
   * @var \Drupal\drd\QueueManager
   */
  protected $queueManager;

  /**
   * @var \Drupal\drd\UpdateProcessor
   */
  protected $updateProcessor;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Action constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Session\AccountInterface $current_user
   * @param \Drupal\drd\SelectEntitiesInterface $entities
   * @param \Drupal\drd\HttpRequest $http_request
   * @param \Drupal\drd\Logging $logging
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\drd\ActionManagerInterface $action_manager
   * @param \Drupal\drd\QueueManager $queue_manager
   * @param \Drupal\drd\UpdateProcessor $update_processor
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, AccountInterface $current_user, SelectEntitiesInterface $entities, HttpRequest $http_request, Logging $logging, EntityTypeManagerInterface $entityTypeManager, FileSystemInterface $file_system, ModuleHandlerInterface $module_handler, ActionManagerInterface $action_manager, QueueManager $queue_manager, UpdateProcessor $update_processor) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
    $this->entities = $entities;
    $this->httpRequest = $http_request;
    $this->logging = $logging;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $file_system;
    $this->moduleHandler = $module_handler;
    $this->actionManager = $action_manager;
    $this->queueManager = $queue_manager;
    $this->updateProcessor = $update_processor;
    $this->setDefaultArguments();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_user'),
      $container->get('drd.entities.select'),
      $container->get('drd.http_request'),
      $container->get('drd.logging'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('module_handler'),
      $container->get('plugin.manager.drd_action'),
      $container->get('queue.drd'),
      $container->get('update.processor.drd')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $terms = [];
    foreach ($this->configuration['terms'] as $id) {
      $terms[] = Term::load($id);
    }
    $form['terms'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'taxonomy_term',
      '#title' => 'Terms',
      '#default_value' => $terms,
      '#tags' => TRUE,
      '#autocreate' => [
        'bundle' => 'tags',
        'uid' => $this->currentUser->id(),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Nothing to do so far.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['terms'] = [];
    foreach ($form_state->getValue('terms') as $item) {
      $item = reset($item);
      if ($item instanceof EntityInterface) {
        if ($item->isNew()) {
          /** @noinspection PhpUnhandledExceptionInspection */
          $item->save();
        }
        $this->configuration['terms'][] = $item->id();
      }
      else {
        $this->configuration['terms'][] = $item;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = NestedArray::mergeDeep(
      $this->defaultConfiguration(),
      $configuration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'terms' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // This is deliberatly empty as we have implemented executeAction with
    // varying signatures.
    // TODO: before we can use DRD actions with rules, we need to change that.
  }

  /**
   * Allows an action to set default arguments.
   */
  protected function setDefaultArguments() {}

  /**
   * {@inheritdoc}
   */
  public function restrictAccess(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function canBeQueued(): bool {
    return TRUE;
  }

  /**
   * Get a list of optional follow-up actions.
   *
   * @return array|string|bool
   *   Return the action key, or a list of action keys, that should follow this
   *   action or FALSE, if no follow-up action required.
   */
  protected function getFollowUpAction() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    if (PHP_SAPI === 'cli' || @ignore_user_abort()) {
      // We are either running via CLI (Drush or Console) or it's a cron run.
      return $return_as_object ? AccessResult::allowed() : TRUE;
    }
    if (!$account) {
      $account = $this->currentUser;
    }
    if ($account->hasPermission($this->getPluginId())) {
      return $return_as_object ? AccessResult::allowed() : TRUE;
    }
    return $return_as_object ? AccessResult::forbidden() : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  final public function setActionArgument($key, $value): BaseInterface {
    $this->arguments[$key] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  final public function setArguments(array $arguments): BaseInterface {
    foreach ($arguments as $key => $value) {
      $this->arguments[$key] = $value;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  final public function getArguments(): array {
    return $this->arguments;
  }

  /**
   * Reset action such that another request can be executed.
   */
  protected function reset() {
    $this->output = NULL;
  }

  /**
   * Add another part to the action output.
   *
   * @param string $output
   *   The new part for the output.
   */
  final protected function setOutput($output) {
    $this->output[] = $output;
  }

  /**
   * {@inheritdoc}
   */
  final public function getOutput() {
    return empty($this->output) ? FALSE : $this->output;
  }

  /**
   * Logging fort actions, forwarding to the DRD logging service.
   *
   * @param string $severity
   *   The message severity.
   * @param string $message
   *   The log message.
   * @param array $args
   *   The log message arguments.
   */
  protected function log($severity, $message, array $args = []) {
    $args['@plugin_id'] = $this->pluginId;
    $this->logging->log($severity, $message, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function hasTerm(Term $term): bool {
    return in_array($term->id(), $this->configuration['terms'], TRUE);
  }

}
