<?php

namespace Drupal\drd\Plugin\Action;

use Exception;

/**
 * Provides a 'ProjectStatus' action.
 *
 * @Action(
 *  id = "drd_action_projects_status",
 *  label = @Translation("Check status for all projects"),
 *  type = "drd",
 * )
 */
class ProjectsStatus extends BaseGlobal {

  /**
   * {@inheritdoc}
   */
  public function executeAction() {
    try {
      $this->updateProcessor->fetchData();
    }
    catch (Exception $ex) {
      return FALSE;
    }
    return TRUE;
  }

}
