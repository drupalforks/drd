<?php

namespace Drupal\drd\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\State;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\drd\ContextProvider\RouteContext;
use Drupal\drd\QueueManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Abstract class for DRD blocks.
 */
abstract class Base extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Route context of current request.
   *
   * @var \Drupal\drd\ContextProvider\RouteContext
   */
  protected $context;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * @var \Drupal\Core\Utility\LinkGeneratorInterface
   */
  protected $linkGenerator;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\drd\QueueManager
   */
  protected $queueManager;

  /**
   * @var \Drupal\Core\State\State
   */
  protected $state;

  /**
   * Base constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountInterface $current_user
   * @param \Drupal\Core\Database\Connection $database
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\drd\QueueManager $queue_manager
   * @param \Drupal\Core\State\State $state
   * @param \Drupal\drd\ContextProvider\RouteContext|null $context
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $current_user, Connection $database, EntityTypeManagerInterface $entity_type_manager, FormBuilderInterface $form_builder, LinkGeneratorInterface $link_generator, ModuleHandlerInterface $module_handler, QueueManager $queue_manager, State $state, $context = NULL) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->context = $context;
    $this->currentUser = $current_user;
    $this->database = $database;
    $this->entityTypeManager = $entity_type_manager;
    $this->formBuilder = $form_builder;
    $this->linkGenerator = $link_generator;
    $this->moduleHandler = $module_handler;
    $this->queueManager = $queue_manager;
    $this->state = $state;
    $this->init();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('form_builder'),
      $container->get('link_generator'),
      $container->get('module_handler'),
      $container->get('queue.drd'),
      $container->get('state'),
      RouteContext::findDrdContext()
    );
  }

  /**
   * Optionallya initialize the instance.
   */
  protected function init() {
    // Can be overwritten.
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

  /**
   * Determine if the current request is within the context of a DRD entity.
   *
   * @return bool
   *   TRUE if context is within a DRD entity.
   */
  protected function isDrdContext(): bool {
    return $this->context ? (bool) $this->context->getEntity() : FALSE;
  }

  /**
   * Get the entity of the current context.
   *
   * @return bool|\Drupal\drd\Entity\BaseInterface
   *   The DRD entity if within an entity context or FALSE otherwise.
   */
  protected function getEntity() {
    return $this->context ? $this->context->getEntity() : FALSE;
  }

}
