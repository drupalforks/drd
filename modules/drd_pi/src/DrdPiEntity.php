<?php

namespace Drupal\drd_pi;

use Drupal;
use Drupal\drd\Entity\BaseInterface;

/**
 * Provides abstract class for platform based entities.
 */
abstract class DrdPiEntity implements DrdPiEntityInterface {

  /**
   * Account to which this entity is attached.
   *
   * @var DrdPiAccountInterface
   */
  protected $account;

  /**
   * Label of this entity.
   *
   * @var string
   */
  protected $label;

  /**
   * ID of this entity.
   *
   * @var string
   */
  protected $id;

  /**
   * DrdEntity which matches this DrdPiEntity.
   *
   * @var \Drupal\drd\Entity\BaseInterface
   */
  protected $entity;

  /**
   * Header values for the DRD entity.
   *
   * @var array
   */
  protected $header = [];

  /**
   * DRD logging service for console output.
   *
   * @var \Drupal\drd\Logging
   */
  protected $logging;

  /**
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * Construct a DrdPiEntity object.
   *
   * @param DrdPiAccountInterface $account
   *   Account to which this entity is attached.
   * @param string $label
   *   Label of this entity.
   * @param string $id
   *   ID of this entity.
   */
  public function __construct(DrdPiAccountInterface $account, $label, $id) {
    $this->account = $account;
    $this->label = $label;
    $this->id = $id;
    $this->logging = Drupal::service('drd.logging');
    $this->httpClientFactory = Drupal::service('http_client_factory');
  }

  /**
   * {@inheritdoc}
   */
  public function id(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function setDrdEntity(BaseInterface $entity): DrdPiEntityInterface {
    $this->entity = $entity;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDrdEntity(): BaseInterface {
    return $this->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function hasDrdEntity(): bool {
    return !empty($this->entity);
  }

  /**
   * {@inheritdoc}
   */
  public function update(): DrdPiEntityInterface {
    if ($this->hasDrdEntity()) {
      $changed = FALSE;
      $foundAuth = FALSE;
      $hasAuth = isset($this->header['Authorization']);
      $headerItems = [];
      /* @var \Drupal\key_value_field\Plugin\Field\FieldType\KeyValueItem $item */
      foreach ($this->entity->get('header') as $item) {
        $value = $item->getValue();
        $isAuth = ($value['key'] === 'Authorization');
        if ($isAuth && !$hasAuth) {
          // Ignore this item.
          $changed = TRUE;
        }
        else {
          if ($isAuth) {
            $foundAuth = TRUE;
            if ($value['value'] !== $this->header['Authorization']) {
              $value['value'] = $this->header['Authorization'];
              $changed = TRUE;
            }
          }
          $headerItems[] = $value;
        }
      }
      if ($hasAuth && !$foundAuth) {
        $headerItems[] = [
          'key' => 'Authorization',
          'value' => $this->header['Authorization'],
        ];
        $changed = TRUE;
      }
      if ($changed) {
        $this->entity->set('header', $headerItems);
        $this->entity->save();
      }
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setHeader($key, $value): DrdPiEntityInterface {
    $this->header[$key] = $value;
    return $this;
  }

}
