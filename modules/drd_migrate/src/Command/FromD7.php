<?php

namespace Drupal\drd_migrate\Command;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Console\Core\Style\DrupalStyle;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\drd\Command\Base;
use Drupal\drd_migrate\Import;
use Exception;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FromD7.
 *
 * @package Drupal\drd
 */
class FromD7 extends Base {

  /**
   * @var \Drupal\drd_migrate\Import
   */
  protected $service;

  /**
   * FromD7 constructor.
   *
   * @param \Drupal\drd_migrate\Import $service
   */
  public function __construct(Import $service) {
    parent::__construct();
    $this->service = $service;
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    parent::configure();
    $this
      ->setName('drd:migrate:from:d7')
      ->setDescription($this->trans('commands.drd.migrate.from.d7.description'))
      ->addArgument(
        'inventory',
        InputArgument::REQUIRED,
        $this->trans('commands.drd.migrate.from.d7.arguments.inventory')
      );
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $io = new DrupalStyle($input, $output);
    try {
      $this->service->execute($input->getArgument('inventory'), $io);
    } catch (InvalidPluginDefinitionException $e) {
      // TODO: Log this exception.
    } catch (PluginNotFoundException $e) {
      // TODO: Log this exception.
    } catch (EntityStorageException $e) {
      // TODO: Log this exception.
    } catch (Exception $e) {
      // TODO: Log this exception.
    }
  }

}
